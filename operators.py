import bpy
from bpy.types import Operator

class OAL_OT_new_layer(Operator):
    """Adds new layer in the NLA"""
    bl_idname = "oal.new_layer"
    bl_label = "Add new NLA layer"
    bl_options = {'REGISTER', 'UNDO'}

    def invoke(self, context, event):
        obj = context.object
        anim_data = obj.animation_data
        #Create new layer if NLA has layers already
        if len(anim_data.nla_tracks) > 0:
            base_track = anim_data.nla_tracks[0]
            new_track = anim_data.nla_tracks.new(prev=anim_data.nla_tracks[obj.active_nla_layer_index])
            new_track.name = "Layer %d" % len(anim_data.nla_tracks)
            new_action = bpy.data.actions.new(name="%s-%s" % (base_track.strips[0].action.name, new_track.name))
            strip = new_track.strips.new(
                name=new_action.name, 
                action=new_action, 
                start=base_track.strips[0].frame_start)
            strip.action_frame_end = base_track.strips[0].action_frame_end
            strip.blend_type = 'ADD'
            obj.active_nla_layer_index += 1
        else:
            base_track = anim_data.nla_tracks.new()
            base_track.name = "Layer 1"
            action = obj.animation_data.action
            strip = base_track.strips.new(
                name=action.name, 
                action=action,
                start=context.scene.frame_start)
            new_track = anim_data.nla_tracks.new(prev=anim_data.nla_tracks[obj.active_nla_layer_index])
            new_track.name = "Layer %d" % len(anim_data.nla_tracks)
            new_action = bpy.data.actions.new(name="%s-%s" % (base_track.strips[0].action.name, new_track.name))
            strip = new_track.strips.new(
                name=new_action.name, 
                action=new_action, 
                start=base_track.strips[0].frame_start)
            strip.action_frame_end = base_track.strips[0].action_frame_end
            strip.blend_type = 'ADD'
            obj.active_nla_layer_index = 1
        return {'FINISHED'}

class OAL_OT_remove_layer(Operator):
    """Removes layer from the NLA"""
    bl_idname = "oal.remove_layer"
    bl_label = "Add new NLA layer"
    bl_options = {'REGISTER', 'UNDO'}

    def invoke(self, context, event):
        obj = context.object
        anim_data = obj.animation_data
        #Only take away if NLA has more than 1 layers
        if len(anim_data.nla_tracks) > 1:
            anim_data.nla_tracks.remove(anim_data.nla_tracks[obj.active_nla_layer_index])
            if obj.active_nla_layer_index == 0:
                obj.active_nla_layer_index += 1
            else:
                obj.active_nla_layer_index -= 1
        else:
            #Don't add to undo stack if we do nothing
            return {'CANCELLED'}

        return {'FINISHED'}