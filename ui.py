from bpy.types import Panel, UIList

class GRAPH_PT_oal_layers(Panel):
    """Creates a Panel in the Graph editor N-Panel"""
    bl_category = "Layers"
    bl_label = "Layers"
    bl_idname = "GRAPH_PT_oal_animation_layers"
    bl_options = {'HIDE_HEADER'}
    bl_space_type = 'GRAPH_EDITOR'
    bl_region_type = 'UI'
    
    def draw(self, context):
        draw_layer_panel(self.layout, context)

class DOPESHEET_PT_oal_layers(Panel):
    """Creates a Panel in the Dopesheet editor N-Panel"""
    bl_category = "Layers"
    bl_label = "Layers"
    bl_idname = "DOPESHEET_PT_oal_animation_layers"
    bl_options = {'HIDE_HEADER'}
    bl_space_type = 'DOPESHEET_EDITOR'
    bl_region_type = 'UI'
    
    def draw(self, context):
        draw_layer_panel(self.layout, context)

#The actual ui function for the layers in the Panel
def draw_layer_panel(layout, context):
    obj = context.object
    col = layout.column()
    # col.prop(obj.animation_data, "action", text="")
    #Layer UI
    col.template_list("NLATRACKS_UL_layer_list", "", obj.animation_data, "nla_tracks", obj, "active_nla_layer_index")
    row = col.row(align=True)
    row.operator("oal.new_layer", text="", icon='NLA')
    row.split()
    row.operator("oal.remove_layer", text="", icon='TRASH')
    #Strip UI
    if len(obj.animation_data.nla_tracks) > 0:
        strip = obj.animation_data.nla_tracks[obj.active_nla_layer_index].strips[0]
        col.prop(strip, "blend_type")
        #Influence doesn't do anything, #TODO: Some sort of hook to actually make it do something??
        # col.prop(strip, "influence")

#How to draw the layer's list
class NLATRACKS_UL_layer_list(UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname):
        preferences = context.preferences.addons[__package__].preferences
        self.use_filter_sort_reverse = True
        # ob = data
        track = item
        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            if track:
                row = layout.row()
                row.prop(track, "mute", text="", emboss=False, icon='HIDE_ON' if track.mute else 'HIDE_OFF')
                row.prop(track, "name", text="", emboss=False, icon_value=icon)
                if preferences.show_action_in_layers:
                    row.prop(track.strips[0].action, "name", text="", emboss=False, icon='ACTION')
                
            else:
                layout.label(text="", translate=False, icon_value=icon)
        elif self.layout_type in {'GRID'}:
            layout.alignment = 'CENTER'
            layout.label(text="", icon_value=icon)

    def draw_filter(self, context, layout):
        row = layout.row(align=True)
        row.prop(self, 'filter_name', text='', icon='VIEWZOOM') 
        row.prop(self, 'use_filter_invert', text='', icon='ARROW_LEFTRIGHT')
